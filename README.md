# Data Structure in Javascript

This project is just for learning about Data structure in Javascript.


# Run

If you just want to run, just get inside the folder and you can use a command which is:

```
node  [name].js
```

or

```
deno run  [name].js
```

__NOTE:__ You must have install node or deno if you want to try any script.

# Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.


# Authors
* **Jean Pierre Giovanni Arenas Ortiz**

# License
[MIT](https://choosealicense.com/licenses/mit/)